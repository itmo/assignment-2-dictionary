%define LABEL_SIZE 8

section .text
%include "lib.inc"

; rdi - указатель на 0-терм. строку - ключ
; rsi - указатель на начало словаря
; Возвращает адрес вхождения. Если его нет, то возвращает 0.
global find_word
find_word:
    xor rax, rax
    .loop:
        push rdi
        push rsi
        lea rsi, [rsi + LABEL_SIZE]
        call string_equals
        pop rsi
        pop rdi
        cmp rax, 1
        je .found
        mov rsi, [rsi]
        test rsi, rsi
        je .not_found
        jmp .loop
    .found:
        mov rax, rsi
        ret
    .not_found:
        mov rax, 0
        ret

; rdi - указатель на вхождение в лист
; Возвращает адрес значения
global get_word
get_word:
    xor rax, rax
    push rdi
    lea rdi, [rdi + LABEL_SIZE]
    call string_length
    pop rdi
    lea rax, [rdi + rax + LABEL_SIZE + 1]  ; изначально указывает на метку размеров 64 бита, потому мы к адресу метки прибавляем 8, то есть label_size
    ret                                    ; далее длину ключа, а далее 1, так как длина ключа без учета 0 терминатора 