%define BUFFER_LENGTH 255

%include "colon.inc"

section .rodata
%include "words.inc"
error_reading_label:
db "There is some problems with reading", 0
error_finding_label:
db "Dictionary doesn't contain the key", 0

section .bss
input: resb BUFFER_LENGTH

section .text
%include "lib.inc"
%include "dict.inc"

global _start
_start:
    mov rdi, input
    mov rsi, BUFFER_LENGTH
    call read_word
    test rax, rax
    je .error_reading
    mov rdi, input
    lea rsi, label3
    call find_word
    test rax, rax
    je .error_finding
    mov rdi, rax
    call get_word
    mov rdi, rax
    call print_string
    mov rdi, 0
    call exit
    .error_reading:
        mov rdi, error_reading_label
        call print_error_string
        call exit
    .error_finding:
        mov rdi, error_finding_label
        call print_error_string
        call exit