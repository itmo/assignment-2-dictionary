%ifndef NEXT_ELEMENT
%define NEXT_ELEMENT 0
%endif

%macro colon 2
%ifstr %1 ;если неверен хотя бы один аргумент макроса, то он не применится
%ifid %2
%2: dq NEXT_ELEMENT
db %1, 0
%define NEXT_ELEMENT %2
%endif
%endif
%endmacro