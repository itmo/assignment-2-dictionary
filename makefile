ASM=nasm
ASM_FLAGS=-f elf64
LINKER=ld

.PHONY: clean
clean:
	rm -f *.o main

.PHONY: update-all
update-all:
	touch colon.inc
	touch dict.asm
	touch dict.inc
	touch lib.asm
	touch lib.inc
	touch main.asm
	touch words.inc

%.o: %.asm
	$(ASM) $(ASM_FLAGS) $^

main: main.o dict.o lib.o dict.inc lib.inc colon.inc words.inc
	$(LINKER) main.o dict.o lib.o -o main

.PHONY: run
run: main
	./main