section .data
numbers: db '0123456789'

section .text

; Принимает код возврата и завершает текущий процесс
global exit
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
global string_length
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
global print_string
print_string:
    push rdi ;сохраняем, т.к caller-saved
    call string_length
    pop rsi ;достаем сохраненное значение rdi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
global print_char
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
global print_uint
print_uint:
    xor r9, r9
    mov rax, rdi ; операции деления будут производиться с rax
    push 0 ; нуль-терминатор
    .find_numbers:
        mov rdx, 0 ;здесь хранится остаток после каждого цикла, обнуляем, ведь rdx является "вершиной" делимого числа числа для div
        mov rsi, 10
        div rsi
        mov rdx, [numbers + rdx]
        bswap rdx
        push rdx
        add rsp, 7
        inc r9
        cmp rax, 0 ;если после деления у нас осталось 0, то число закончилось, можно выводить
        je .print_number
        jmp .find_numbers
    .print_number:
        mov rdi, rsp
        push r9
        call print_string
        pop r9
        add rsp, r9 - 1
        pop rdi
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
global print_int
print_int:
    mov rsi, rdi
    cmp rsi, 0
    jns .print_number
    push rdi ; caller-saved - сохраняю
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .print_number:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
global string_equals
string_equals:
    xor rdx, rdx
    .loop:
        mov r8b, [rsi + rdx]
        cmp byte[rdi + rdx], r8b
        jne .not_equals
        cmp byte[rdi + rdx], 0
        je .equals
        inc rdx
        jmp .loop
    .not_equals:
        mov rax, 0
        ret
    .equals:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
    push 0
    mov rsi, rsp
    mov rdx, 1
    mov rax, 0
    mov rdi, 0
    syscall
    cmp rax, 0
    je .EOF
    mov rax, [rsp]
    add rsp, 8
    ret 
    .EOF:
        add rsp, 8
        ret

is_space_symbol:
    xor rax, rax
    cmp rdi, ' '
    je .true
    cmp rdi, `\t`
    je .true
    cmp rdi, `\n`
    je .true
    ret
    .true:
        mov rax, 1
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
global read_word
read_word:
    push rbx
    push rbp
    push r12
    xor rbx, rbx ;счетчик
    mov rbp, rdi ;адрес начала
    mov r12, rsi ;размер буфера
    .loop_first:
        cmp r12, 1
        jb .EOF

        call read_char

        cmp rax, 0 ;проверка на конец ввода
        je .end

        push rax ;проверка на пробельные смиволы
        mov rdi, rax
        call is_space_symbol
        cmp rax, 1
        pop rax
        je .loop_first

        mov byte[rbp + rbx], al
        inc rbx
    .loop_second:
        cmp rbx, r12
        jae .EOF

        call read_char

        cmp rax, 0 ;проверка на конец ввода
        je .end
    
        push rax ;проверка на пробельные смиволы
        mov rdi, rax
        call is_space_symbol
        cmp rax, 1
        pop rax
        je .end

        mov byte[rbp + rbx], al
        inc rbx
        jmp .loop_second
    .end:
        mov byte[rbp + rbx], 0
        mov rax, rbp
        mov rdx, rbx
        jmp .ret
    .EOF:
        mov rdx, 0
        mov rax, 0
    .ret:
        pop r12
        pop rbp
        pop rbx
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
global parse_uint
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor rsi, rsi
    .loop:
        mov sil, byte[rdi + rdx]
        
        cmp sil, '0' ; проверки на то, что ввод в пределах [0-9]
        jb .end
        cmp sil, '9'
        ja .end

        sub sil, 48
        mov rcx, rax
        sal rcx, 1
        sal rax, 3
        add rax, rcx
        add rax, rsi
        inc rdx
        jmp .loop
    .end:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
global parse_int
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    je .signed
    .unsigned:
        jmp parse_uint
    .signed:
        inc rdi
        call parse_uint
        cmp rdx, 0
        jbe .end
        neg rax
        inc rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
global string_copy
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        jae .error
        mov r8b, byte[rdi + rax]
        mov byte[rsi + rax], r8b
        cmp byte[rsi + rax], 0
        je .end
        inc rax
        jmp .loop
    .error:
        xor rax, rax
        ret
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
global print_error_string
print_error_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 2
    syscall
    ret